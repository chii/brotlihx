package;
class Host {
  static public function main():Void {
    trace('Hello from cppia HOST : ${Sys.args()}');
    var scriptname = Sys.args()[0];  
    var contents = sys.io.File.getContent(scriptname);
    cpp.cppia.Host.run(contents);  // <- load and execute the .cppia script file 
  }
}
