package ;

import haxe.*;
import haxe.unit.*;
import haxe.io.*;
import brotli.*;

#if python
class TestBrotli {
    public static function main():Void {
        var serializer = new Serializer();
        serializer.useCache = true;
        var m = new Map<Int, Bool>();
        for (i in 0 ... 1000) {
            m.set(i, i % 2 == 0);
        }
        serializer.serialize(m);
        var uncompressed = serializer.toString();
        trace('uncompressed: ${uncompressed.length}');
        // trace('uncompressed: ${uncompressed}');
        var brotli = new Brotli();
        for (quality in 1 ... 12) {
            try {
                var t_start = Date.now().getTime();
                var compressed = brotli.compress(uncompressed, quality);
                var t_end = Date.now().getTime();
                if (compressed.length < uncompressed.length) {
                    throw 'failed: compressed.length < uncompressed.length';
                }
                trace('quality $quality compressed ${t_end - t_start}ms: ${compressed.length}');
                // trace('compressed : ${compressed}');
                t_start = Date.now().getTime();
                var decompressed = brotli.decompress(compressed);
                t_end = Date.now().getTime();
                if(uncompressed.length != decompressed.length) {
                    throw 'failed: uncompressed.length != decompressed.length';
                }
                trace('quality $quality decompressed ${t_end - t_start}ms: ${decompressed.length}');

                // trace('decompressed : ${decompressed}');
                var unserializer = new Unserializer(decompressed);
                var m_deserialized:Map<Int,Bool> = unserializer.unserialize();
                for (j in 0 ... 1000) {
                    if (m_deserialized.get(j) != m.get(j)) {
                        throw 'failed: $j m_deserialized.get(j) != m.get(j)';
                    }
                }
            } catch (ex:Dynamic) {
                trace('${ex}');
            }
        }
    }
}
#else
class TestBrotli extends TestCase {
    public function testBasicBrotliCompression() {
        var serializer = new Serializer();
        serializer.useCache = true;
        var m = new Map<Int, Bool>();
        for (i in 0 ... 5000) {
            m.set(i, i % 2 == 0);
        }
        serializer.serialize(m);
        // var customClass = new TestDataClass(1, 2, {myfield:"some_value"});
        // serializer.serialize(customClass)
        var uncompressed = serializer.toString();
        trace('uncompressed: ${uncompressed.length}');
        // trace('uncompressed: ${uncompressed}');
        var brotli = new Brotli();
        for (quality in 1 ... 12) {
            // try {
                
            // } catch (ex:Dynamic) {
            //     trace('${ex}:\n\t${ex.error}\n\t\t${ex.posInfos}');
            // }
            var t_start = Date.now().getTime();
            var compressed = brotli.compress(uncompressed, quality);
            var t_end = Date.now().getTime();
            assertTrue(compressed.length < uncompressed.length);
            var ratio:Float = (compressed.length/1.0) / uncompressed.length;
            trace('quality $quality compressed ${t_end - t_start}ms: ${compressed.length} : ratio: ${ratio}');
            // trace('compressed : ${compressed}');
            t_start = Date.now().getTime();
            var decompressed = brotli.decompress(compressed);
            t_end = Date.now().getTime();
            assertEquals(uncompressed.length, decompressed.length);
            trace('quality $quality decompressed ${t_end - t_start}ms: ${decompressed.length}');

            // trace('decompressed : ${decompressed}');
            var unserializer = new Unserializer(decompressed);
            var m_deserialized:Map<Int,Bool> = unserializer.unserialize();
            for (j in 0 ... 5000) {
                assertEquals(m_deserialized.get(j), m.get(j));
            }
        }
    }

    public static function main():Void {
        var r = new TestRunner();
        r.add(new TestBrotli());
        // add other TestCases here

        // finally, run the tests
        r.run();
    }
}
#end
class TestDataClass {
    var myfield1:Int;
    var myfield2:Int;
    var myfield3:Dynamic;
    public function new(myfield1, myfield2, myfield3) {
        this.myfield1 = myfield1;
        this.myfield2 = myfield2;
        this.myfield3 = myfield3;
    }
}
