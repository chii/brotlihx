package brotli.encode.static_dict_lut;

/**
 * ...
 * @author 
 */
class DictWord
{

	public var len:Int;
  public var transform:Int;
  public var idx:Int;
	public function new(len:Int,transform:Int,idx:Int) 
	{
		this.len = len;
		this.transform = transform;
		this.idx = idx;
	}
	
}