package brotli.encode.entropy_encode;
import haxe.ds.Vector;

/**
 * ...
 * @author 
 */
class EntropyCode
{

	public function new(kSize:Int) 
	{
		depth_ = FunctionMalloc.mallocInt(kSize);
		bits_ = FunctionMalloc.mallocInt(kSize);
	}
	
  // How many bits for symbol.
	public var depth_:Vector<Int>;
  // Actual bits used to represent the symbol.
  public var bits_:Vector<Int>;
  // How many non-zero depth.
	public var count_:Int;
  // First four symbols with non-zero depth.
  public var symbols_:Vector<Int>=FunctionMalloc.mallocInt(4);
}