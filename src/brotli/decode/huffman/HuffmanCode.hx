package brotli.decode.huffman;

/**
 * ...
 * @author 
 */
class HuffmanCode
{

	public var bits:Int;//Int8_t     /* number of bits used for this symbol */
	public var value:Int;//Int16_t   /* symbol value or table offset */
	public function new(bits,value) 
	{
		this.bits = bits;
		this.value = value;
	}
	
}