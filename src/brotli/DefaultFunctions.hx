package brotli;
import haxe.ds.Vector;
/*@:coreType abstract Size_t from Int { }
@:coreType abstract Int8_t from Int { }
@:coreType abstract Int16_t from Int { }
@:coreType abstract Int32_t from Int { }
@:coreType abstract Int64_t from Int { }*/

/**
 * ...
 * @author ...
 */
class DefaultFunctions
{
	@:generic public static function memset<T>(b:Vector<T>, offset:Int, v:T, count) {
		for (i in 0...count)
		b[offset+i] = v;
	}
	@:generic public static function memcpy<T>(dst:Vector<T>, dst_offset:Int, src:Vector<T>, src_offset:Int, count) {
		for (i in 0...count)
		dst[dst_offset+i] = src[src_offset+i];
	}
	public static function memcpyArray(dst:Array<Int>, dst_offset:Int, src:Array<Int>, src_offset:Int, count) {
		for (i in 0...count)
		dst[dst_offset+i] = src[src_offset+i];
	}
	public static function memcpyVectorArray(dst:Vector<Int>, dst_offset:Int, src:Array<Int>, src_offset:Int, count) {
		for (i in 0...count)
		dst[dst_offset+i] = src[src_offset+i];
	}
	public static function memcpyArrayVector(dst:Array<Int>, dst_offset:Int, src:Vector<Int>, src_offset:Int, count) {
		for (i in 0...count)
		dst[dst_offset+i] = src[src_offset+i];
	}


	public function new() 
	{
		
	}
	
}