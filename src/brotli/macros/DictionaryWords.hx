package brotli.macros;
#if !java
@:build(brotli.macros.DictMacros.buildDictionary())
class DictionaryWords {
}
#else
class DictionaryWords {
    public static var contents:String = haxe.Resource.getString("DictionaryWords");
}
#end