package brotli.macros;

import haxe.io.Path;
import haxe.macro.Context;
import haxe.macro.Expr;
import haxe.macro.Type;
import sys.FileSystem;
import sys.io.File;
import haxe.ds.Vector;
import haxe.io.Bytes;

class DictMacros {
  public static function buildDictionary():Array<Field> {
    // get the current fields of the class
    var fields:Array<Field> = Context.getBuildFields();
    
    // get the path of the current current class file, e.g. "src/path/to/MyClassName.hx"
    var posInfos = Context.getPosInfos(Context.currentPos());
    var directory = Path.directory(posInfos.file);
    
    // get the current class information. 
    var ref:ClassType = Context.getLocalClass().get();
    // path to the template. syntax: "MyClassName.template"
    var filePath:String = Path.join([directory, ref.name + ".txt"]);
    
    // detect if template file exists
    if (FileSystem.exists(filePath)) {
      // get the file content of the template 
      //var fileContent:String = File.getContent(filePath);
      
      var input = File.read(filePath, true);
      var bytes = input.readAll();
      var content:Vector<Int> = new Vector<Int>(bytes.length);
      for (i in 0...bytes.length) {
        content[i] = bytes.get(i) & 0xff;
      }
      input.close;

      var exprs = [for(value in content) macro $v{value}];

      // add a static field called "TEMPLATE" to the current fields of the class
      fields.push({
        name:  'contents',
        access:  [Access.AStatic, Access.APublic],
        kind: FieldType.FVar(macro:Array<Int>,macro $a{exprs}), 
        pos: Context.currentPos(),
        doc: "auto-generated from " + filePath,
      });
    }
    
    return fields;
  }
}