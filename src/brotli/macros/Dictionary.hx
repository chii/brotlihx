package brotli.macros;
#if !java
@:build(brotli.macros.DictMacros.buildDictionary())
class Dictionary {
}
#else
class Dictionary {
    public static var contents:String = haxe.Resource.getString("Dictionary");
}
#end