package brotli.macros;
#if !java
@:build(brotli.macros.DictMacros.buildDictionary())
class DictionaryHash {
}
#else
class DictionaryHash {
    public static var contents:String = haxe.Resource.getString("DictionaryHash");
}
#end