package brotli.macros;
#if !java
@:build(brotli.macros.DictMacros.buildDictionary())
class DictionaryBuckets {
}
#else
class DictionaryBuckets {
    public static var contents:String = haxe.Resource.getString("DictionaryBuckets");
}
#end